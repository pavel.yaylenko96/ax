const { faker } = require('@faker-js/faker');
const axios = require('axios').default;
var expect = require('expect.js');
var describe = require('mocha').describe;

const rEmail = faker.internet.email();
console.log(rEmail)
var token;
describe('', function () {
    it('a', () => {
        axios.post('https://bbb.testpro.io/registration.php', {
            email: rEmail
        })
            .then(function (response) {
                // console.log(typeof(JSON.parse(response.headers)))
                console.log(response.headers['cf-cache-status'])
                expect(response.headers).to.have.property('cf-cache-status')
                expect(response.headers['cf-cache-status']).to.be.equal('DYNAMIC')
                expect(response.status).to.be.equal(200)
                // var headers = JSON.stringify(response.headers)
                // console.log(typeof (headers))
                // console.log(headers.data)
            })
            .catch(function (error) {
                console.log(error);

            });
    })

    it('b', () => {
        axios.post('https://bbb.testpro.io/api/me',
            {
                email: rEmail,
                password: 'te$t$tudent'
            }
        ).then(function (response) {
            console.log(response.body);
        })
            .catch(function (error) {
                console.log(error);
            });

    })
})